# PHP para quem entende PHP

Estes são apenas cursos básicos para aprimorar meus conhecimentos.

Os cursos são gratuitos e estão na Udemy.

### Link para os cursos

- PHP para quem entende PHP => https://www.udemy.com/php-para-quem-entende-php/
- PDO para quem não sabe PDO => https://www.udemy.com/pdo-para-quem-nao-sabe-pdo/