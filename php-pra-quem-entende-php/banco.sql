CREATE DATABASE banco;

USE banco;

CREATE TABLE users(
    id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    last_name VARCHAR(255) NOT NULL,
    email VARCHAR(255),
-- Eu sei que o password está em varchar e isto esta errado, mas o intuito do curso era outro.
    password VARCHAR(100) NOT NULL
);