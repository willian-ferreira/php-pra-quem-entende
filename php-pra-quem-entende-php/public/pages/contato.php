<h2>Contato</h2>

<?= get('message'); ?>

<form action="/pages/forms/contato.php" method="POST" role="form">
    
    <div class="form-group">
        <label for="name">Nome</label>
        <input type="text" name="name" id="name" class="form-control" placeholder="Digiteu seu nome">
    </div>

    <div class="form-group">
        <label for="email">Email</label>
        <input type="text" name="email" id="email" class="form-control" placeholder="Digiteu seu email">
    </div>

    <div class="form-group">
        <label for="subject">Assunto</label>
        <input type="text" name="subject" id="subject" class="form-control" placeholder="Digiteu seu assunto">
    </div>
    
    <div class="form-group">
        <label for="message">Mensagem</label>
        <textarea name="message" id="message" cols="30" rows="10" class="form-control" placeholder="Digiteu sua mensagem"></textarea>
    </div>

    <button type="submit" class="btn btn-primary">Enviar</button>
</form>