<?= get('message') ?>
<?php
    $user = find('users', 'id', $_GET['id']);
?>
<form action="/pages/forms/update_user.php" method="POST" role="form">
    
    <input type="hidden" name="id" value="<?= $user->id; ?>">

    <div class="form-group">
        <label for="name">Nome</label>
        <input type="text" name="name" id="name" class="form-control" placeholder="Digiteu seu nome" value="<?= $user->name; ?>">
    </div>

    <div class="form-group">
        <label for="last_name">Sobrenome</label>
        <input type="text" name="last_name" id="last_name" class="form-control" placeholder="Digiteu seu sobrenome" value="<?= $user->last_name; ?>">
    </div>

    <div class="form-group">
        <label for="email">Email</label>
        <input type="text" name="email" id="email" class="form-control" placeholder="Digiteu seu email" value="<?= $user->email; ?>">
    </div>
    
    <div class="form-group">
        <label for="password">Password</label>
        <input type="password" name="password" id="password" class="form-control" placeholder="Digiteu sua senha" value="<?= $user->password; ?>">
    </div>

    <button type="submit" class="btn btn-primary">Atualizar</button>
</form>