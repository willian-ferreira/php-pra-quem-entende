<?php

require '../../../bootstrap.php';

if(isEmpty()) {
    flash('message', 'Preencha todos os campos!');

    redirect('contato');
}

$validate = validate([
    'name' => 's',
    'email' => 'e',
    'subject' => 's',
    'message' => 's'
]);

$data = [
    'quem' => $validate->email,
    'para' => 'ferreira.willian12@gmail.com',
    'mensagem' => $validate->message,
    'assunto' => $validate->subject
];

if(sendPhpMailer($data)) {
    flash('message', 'Email enviado com sucesso!', 'success');

    redirect('contato');
} else {
    flash('message', 'Erro ao enviar email!');

    redirect('contato');
}