<?php 

require '../../../bootstrap.php';

if(isEmpty()) {
    flash('message', 'Preencha todos os campos!');

    redirect('create_user');
}

$validate = validate([
    'name' => 's',
    'last_name' => 's',
    'email' => 'e',
    'password' => 's'
]);

$cadastrado = create('users', $validate);

if($cadastrado) {
    flash('message', 'Cadastrado com sucesso!', 'success');

    redirect('create_user');
}

flash('message', 'Erro ao cadastrar!');

redirect('create_user');