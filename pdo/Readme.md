## Trabalhando com transactions

Quando trabalhar com mais de uma query junta, sempre usar a mesma conexão.
Antes de fazer a query chama o metodo "beginTransaction" do PDO, depois exececuta as query. Se uma das querys falhar, é so usar o metodo "rollback", e ser der certo, usar o "commit"

### Exemplo

```php
$pdo = new PDO;

$pdo->beginTransaction();

try {

    $user->prepare("INSERT ...");

    $post->prepare("UPDATE ...");

    $pdo->commit();

} catch (Exception $e) {

    $pdo->rollback;

}

```