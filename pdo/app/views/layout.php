<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PDO Pra quem não sabe PDO</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bulma.min.css">
</head>
<body>

    <nav class="navbar is-link" role="navigation" aria-label="main navigation">
        <div class="navbar-brand">
            <a class="navbar-item" href="/">
                Início 
            </a>
            <a class="navbar-item" href="/user_create">
                Cadastrar User 
            </a>
        </div>
    </nav>


    <div class="container" style="margin-top: 20px">
    
        <?php require $layout->load(); ?>

    </div>

</body>
</html>