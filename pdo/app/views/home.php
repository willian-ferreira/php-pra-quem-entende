<h2>Home</h2>

<table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
    <thead>
        <tr>
            <th>Id</th>
            <th>Nome</th>
            <th>Email</th>
            <th style="width: 10%"></th>
            <th style="width: 10%"></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($users as $user): ?>
        <tr>
            <td> <?= $user->id; ?> </td>
            <td> <?= $user->name; ?> </td>
            <td> <?= $user->email; ?> </td>
            <td class="has-text-centered">
                <a href="/user_edit?id=<?= $user->id ?>"  class="button is-link is-small"> 
                    Editar
                </a>
            </td>
            <td class="has-text-centered">
                <a href="/user_destroy?id=<?= $user->id ?>"  class="button is-danger is-small"> 
                    Deletar
                </a>
            </td>
        </tr>
    <?php endforeach; ?>    
    </tbody>
</table>