<?php 

/* DD COM VAR_DUMP
function dd($dump) {
    var_dump($dump);
    die();
}*/

/**
 * dieDump com print_r
 * 
 * $param Any $dump;
 */
function dd($dump) {    
    echo "<div>";
        echo "<pre>";
            print_r($dump);
        echo "</pre>";
    echo "</div>";
    die();
}