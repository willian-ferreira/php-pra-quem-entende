<?php

namespace app\classes;

/**
 * Essa classe é um container para passar para aplicação a mesma
 * instância da conexão
 */
class Bind 
{

    private static $bind = [];
    
    /**
     * Passa para o $bind em qual posição, vai estar o valor, para ser
     * resgatado no container
     * 
     * @param String $key 
     * @param String $value 
     */
    public static function bind($key, $value)
    {
        static::$bind[$key] = $value;
    }

    /**
     * Resgata o valor (Conexão) do container a partir do índice
     * 
     * @param String $key
     */
    public static function get($key)
    {
        return static::$bind[$key];
    }

}