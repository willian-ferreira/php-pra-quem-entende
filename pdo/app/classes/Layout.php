<?php

namespace app\classes;

class Layout 
{

    private $view;

    /**
     * Adiciona uma view para view global dessa classe;
     * 
     * @param string $view
     */
    public function add($view) 
    {
        $this->view = $view;
    }

    /**
     * Carrega a view passada pelo add;
     * @return file.php
     */
    public function load() 
    {
        return "../app/views/{$this->view}.php";
    }

    /**
     * Carrega o layout master;
     * @return file.php
     */
    public function master($master) 
    {
        return "../app/views/{$master}.php";
    }

}