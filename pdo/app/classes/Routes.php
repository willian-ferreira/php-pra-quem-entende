<?php

namespace app\classes;

class Routes {

    /**
     * Carrega a controller de acordo com a rota passada por parâmetro
     * 
     * @param array $routes;
     * @param key $uri;
     * @return file.php
     */
    public static function load($routes, $uri) {
        if(!array_key_exists($uri, $routes)) {
            throw new \Exception("Rota não existe ${$uri}");
        }

        return "../app/{$routes[$uri]}.php";
    }

}