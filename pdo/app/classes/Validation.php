<?php

namespace app\classes;

class Validation 
{

    private $validate = [];

    /**
     * Valida os campos do form, caso tem algo errado, como
     * campos com scripts maliciosos, etc... 
     * 
     * @param Array $post;
     * @return Object $validate transformado em objeto com todos 
     * campos validados; 
     */
    public function validate($post) 
    {
        foreach ($post as $key => $value) {
            $this->validate[$key] = filter_var($value, FILTER_SANITIZE_STRING);
        }

        return (object) $this->validate;
    }

}