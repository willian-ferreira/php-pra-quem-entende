<?php

namespace app\models\QueryBuilder;

class Update {

	private $where;

	/**
	 * Retorna o where para monstar a query
	 * 
	 * @param String $where
	 * @return Function $this 
	 */
	public function where($where) {
		$this->where = $where;

		return $this;
	}

	/**
     * Monta o SQL pra update em todos os models
     * 
     * @param String $table
     * @param Array $attributes
     * @return String retorna o SQL de update montado
     */
	public function sql($table, $attributes) {
		$sql = "UPDATE {$table} SET ";

		unset($attributes[array_keys($this->where)[0]]);

		foreach ($attributes as $key => $value) {
			$sql .= "{$key} = :{$key}, ";
		}

		$sql = rtrim($sql, ', ');
		$where = array_keys($this->where);
		$sql .= " WHERE {$where[0]} = :{$where[0]}";

		return $sql;
	}
}