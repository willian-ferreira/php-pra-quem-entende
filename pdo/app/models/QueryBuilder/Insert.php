<?php

namespace app\models\QueryBuilder;

class Insert {

    /**
     * Monta o SQL pra insert em todos os models
     * 
     * @param String $table
     * @param Array $attributes
     * @return String retorna o SQL de insert montado
     */
    public static function sql($table, $attributes) {
        $sql = "INSERT INTO {$table}(";

        $sql .= implode(', ', array_keys($attributes)) . ') values (';
        $sql .= ':' . implode(', :', array_keys($attributes)) . ')';

        return $sql;
    }
}
