<?php

namespace app\models;

use \PDO;

class Connection {

    /**
     * Cria a conexão com o banco
     * 
     * @return PDO;
     */
    public static function connect() {
        $config = require '../config.php';

        try {
            $pdo = new PDO(
                "mysql:host={$config['db']['host']};
                dbname={$config['db']['name']};
                charset={$config['db']['charset']}", 
                $config['db']['username'], 
                $config['db']['password'] 
            );
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);

            return $pdo;

        } catch(\PDOException $exception) {
            return $exception->getMessage();
            
        }
    }

}