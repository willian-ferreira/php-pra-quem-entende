<?php

namespace app\models;

use app\classes\Bind;
use app\traits\PersistDb;

abstract class Model {

    use PersistDb;

    protected $connection;

    public function __construct() {
        /* $this->connection = Connection::connect(); */
        $this->connection = Bind::get('connection');
    }

    /**
     * Busca todos os registros de uma tabela
     */
    public function all() {
        $query = $this->connection->prepare("SELECT * FROM {$this->table}");
        $query->execute();

        return $query->fetchAll();
    }

    /**
     * Busca apenas um registro
     */
    public function find($field, $value) {
        $query = $this->connection->prepare("SELECT * FROM {$this->table} WHERE {$field} = ?");
        $query->bindValue(1, $value);
        $query->execute();

        return $query->fetch();
    }

    /**
     * Deleta um registro
     */
    public function delete($field, $value) {
        $query = $this->connection->prepare("DELETE * FROM {$this->table} WHERE $field = ?");
        $query->bindValue(1, $value);
        $query->execute();

        return $query->rowCount();
    }
}