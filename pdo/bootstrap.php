<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require 'vendor/autoload.php';
require 'app/functions/helper.php';

app\classes\Bind::bind('connection', app\models\Connection::connect());